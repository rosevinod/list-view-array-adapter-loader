package ds.com.listviewarrayadapterdemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


/**
 * A custom ArrayAdapter used by the {@link ds.com.listviewarrayadapterdemo.MainActivity.AppListFragment} to display the
 * device's installed applications.
 */
public class AppListAdapter extends ArrayAdapter<String> {
  private LayoutInflater mInflater;

  Context context;
  public AppListAdapter(Context ctx) {
    super(ctx, android.R.layout.simple_list_item_2);
    this.context = ctx;
    mInflater = (LayoutInflater) ctx
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View view;

    if (convertView == null) {
      view = mInflater.inflate(R.layout.list_item_icon_text, parent, false);
    } else {
      view = convertView;
    }

    String item = getItem(position);

    System.out.println("Saved data "+item);

    ((ImageView) view.findViewById(R.id.icon))
            .setImageDrawable(this.context.getResources().getDrawable(R.drawable.ic_launcher));

    ((TextView) view.findViewById(R.id.text)).setText(item);

    return view;
  }

  /** Save the data into ArrayAdapter<String> using add method
   * This data coming from onLoadFinished of MainActivity
   * */

  public void setData(List<String> data) {
      System.out.println("Data from App List Loader "+data);
    clear();
    if (data != null) {
      for (int i = 0; i < data.size(); i++) {
        add(data.get(i));
      }
    }
  }
}
